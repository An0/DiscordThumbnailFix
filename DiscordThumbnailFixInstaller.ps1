﻿$ErrorActionPreference = "Stop"

function AddExtension([string]$electonDataPath) {
	'adding extension'
	$extensionListPath = "$electonDataPath\DevTools Extensions"
	if(Test-Path $extensionListPath) {
		$extensionList = ConvertFrom-Json (Get-Content $extensionListPath)
		$extensionList = @($extensionList | ? { $_ -notmatch '(?:^|[\\\/])DiscordThumbnailFix[\\\/]?$' })
		if($extensionList.Length -ne 0) {
			$extensionList += '../../DiscordThumbnailFix'
			Set-Content $extensionListPath (ConvertTo-Json $extensionList)
			return
		}
	}
	Set-Content $extensionListPath '["../../DiscordThumbnailFix"]'
}

function StopProcesses([string]$name, [string]$root) {
	$targets = Get-Process | ? { $_.Name -eq $name -or ($_.Path -and $_.Path.StartsWith($root)) }
	if($targets.Length -eq 0) { return }
    try {
    	$targets | Stop-Process
    } catch { "PLEASE CLOSE $name!" }
	"waiting for $name to close"
	do {
		sleep 1
		$targets = Get-Process | ? { $_.Name -eq $name -or ($_.Path -and $_.Path.StartsWith($root)) }
	} while($targets.Length -gt 0)
}

$discordPath = $env:LOCALAPPDATA+'\Discord'
$discordDataPath = $env:APPDATA+'\discord'
$discordResourcesPath = $discordPath+'\app-*\resources'
$discordPtbPath = $env:LOCALAPPDATA+'\DiscordPTB'
$discordPtbDataPath = $env:APPDATA+'\discordptb'
$discordPtbResourcesPath = $discordPtbPath+'\app-*\resources'
$discordCanaryPath = $env:LOCALAPPDATA+'\DiscordCanary'
$discordCanaryDataPath = $env:APPDATA+'\discordcanary'
$discordCanaryResourcesPath = $discordCanaryPath+'\app-*\resources'
$pluginPath = $env:LOCALAPPDATA+'\DiscordThumbnailFix'


$install = $false

try {

if(Test-Path $discordPath) {
	'Discord found'
	if(Test-Path $discordDataPath) { 'data directory found' } else { 'data directory not found'; return }
	if(Test-Path $discordResourcesPath) { 'resources directory found' } else { 'resources directory not found'; return }
	
	StopProcesses 'Discord' $discordPath

	AddExtension $discordDataPath

	$install = $true
}

if(Test-Path $discordPtbPath) {
	'DiscordPTB found'
	if(Test-Path $discordPtbDataPath) { 'data directory found' } else { 'data directory not found'; return }
	if(Test-Path $discordPtbResourcesPath) { 'resources directory found' } else { 'resources directory not found'; return }
	
	StopProcesses 'DiscordPTB' $discordPtbPath

	AddExtension $discordPtbDataPath

	$install = $true
}

if(Test-Path $discordCanaryPath) {
	'DiscordCanary found'
	if(Test-Path $discordCanaryDataPath) { 'data directory found' } else { 'data directory not found'; return }
	if(Test-Path $discordCanaryResourcesPath) { 'resources directory found' } else { 'resources directory not found'; return }
	
	StopProcesses 'DiscordCanary' $discordCanaryPath
	
	AddExtension $discordCanaryDataPath

	$install = $true
}


if($install) {
	'installing'

	[void](New-Item "$pluginPath\manifest.json" -Type File -Force -Value @'
{
	"name": "DiscordThumbnailFix",
	"content_scripts": [ {
		"js": [ "DiscordThumbnailFixLoader.js" ],
		"matches": [ "*" ],
		"run_at": "document_start"
	} ]
}
'@)

	[void](New-Item "$pluginPath\DiscordThumbnailFixLoader.js" -Type File -Force -Value @'
// ==UserScript==
// @name         DiscordThumbnailFix
// @namespace    https://gitlab.com/An0/DiscordThumbnailFix
// @version      1.0.0
// @description  This should fix discord's shitty thumbnail quality
// @author       An0
// @license      LGPLv3 - https://www.gnu.org/licenses/lgpl-3.0.txt
// @downloadURL  https://gitlab.com/An0/DiscordThumbnailFix/raw/master/DiscordThumbnailFix.user.js
// @updateURL    https://gitlab.com/An0/DiscordThumbnailFix/raw/master/DiscordThumbnailFix.meta.js
// @match        https://discordapp.com/channels/*
// @match        https://discordapp.com/activity
// @match        https://ptb.discordapp.com/channels/*
// @match        https://ptb.discordapp.com/activity
// @match        https://canary.discordapp.com/channels/*
// @match        https://canary.discordapp.com/activity
// @grant        unsafeWindow
// ==/UserScript==

(function() {

'use strict';

const BaseColor = "#fc0";

var Discord;
var Utils = {
    Log: (message) => { console.log(`%c[DiscordThumbnailFix] %c${message}`, `color:${BaseColor};font-weight:bold`, "") },
    Warn: (message) => { console.warn(`%c[DiscordThumbnailFix] %c${message}`, `color:${BaseColor};font-weight:bold`, "") },
    Error: (message) => { console.error(`%c[DiscordThumbnailFix] %c${message}`, `color:${BaseColor};font-weight:bold`, "") }
};

function Init(nonInvasive)
{
    Discord = { window: (typeof(unsafeWindow) !== 'undefined') ? unsafeWindow : window };

    if(Discord.window.webpackJsonp == null) { if(!nonInvasive) Utils.Error("Webpack not found."); return 0; }

    const webpackExports = typeof(Discord.window.webpackJsonp) === 'function' ?
          Discord.window.webpackJsonp(
              [],
              { '__extra_id__': (module, _export_, req) => { _export_.default = req } },
              [ '__extra_id__' ]
          ).default :
          Discord.window.webpackJsonp.push( [
              [],
              { '__extra_id__': (_module_, exports, req) => { _module_.exports = req } },
              [ [ '__extra_id__' ] ] ]
          );

    delete webpackExports.m['__extra_id__'];
    delete webpackExports.c['__extra_id__'];

    const findModule = (filter, nonInvasive) => {
        for(let i in webpackExports.c) {
            if(webpackExports.c.hasOwnProperty(i)) {
                let m = webpackExports.c[i].exports;

                if(!m) continue;

                if(m.__esModule && m.default) m = m.default;

                if(filter(m)) return m;
            }
        }

        if (!nonInvasive) {
            console.warn("Couldn't find module in existing cache. Loading all modules.");

            for (let i = 0; i < webpackExports.m.length; i++) {
                try {
                    let m = webpackExports(i);

                    if(!m) continue;

                    if(m.__esModule && m.default) m = m.default;

                    if(filter(m)) return m;
                }
                catch (e) { }
            }

            console.warn("Cannot find module.");
        }

        return null;
    };

    const findModuleByUniqueProperties = (propNames, nonInvasive) => findModule(module => propNames.every(prop => module[prop] !== undefined), nonInvasive);

    let imageProxyModule = findModuleByUniqueProperties([ 'loadImage', 'isImageLoaded', 'getImageSrc' ], nonInvasive);
    if(imageProxyModule == null) { if(!nonInvasive) Utils.Error("imageProxyModule not found."); return 0; }

    const lossyMediaRegex = /^https:\/\/[^.]+\.discordapp\.net\/.*\.(?:jpe?g|webp)$/i; //unsure about gif

    let original_getImageSrc = imageProxyModule.getImageSrc;
    imageProxyModule.getImageSrc = function(url, width, height, scale) {
        if(lossyMediaRegex.test(url)) {
            arguments[3] = Math.min(scale * 2, 1);
        }

        return original_getImageSrc.apply(this, arguments);
    };

    Utils.Log("loaded");

    return 1;
}


function TryInit()
{
    if(Init(true) !== 0) return;

    window.setTimeout(TryInit, 100);
};


TryInit();

})();

'@)

	'FINISHED'
}
else { 'Discord not found' }

}
catch { $_ }
finally { [Console]::ReadLine() }