# DiscordThumbnailFix

Recently Discord lowered the quality of jpeg (and webp) thumbnails majorly (I don't understand how so few people notice)<br>
<br>
This plugin should fix that problem<br>
<br>
<br>
Install it as a [userscript](https://gitlab.com/An0/DiscordThumbnailFix/raw/master/DiscordThumbnailFix.user.js) or include the js file somehow else<br>
If you have Discord installed, use the [installer](https://gitlab.com/An0/DiscordThumbnailFix/raw/master/DiscordThumbnailFixInstaller.ps1) (Alt+click to download, right click on the downloaded file and Run with PowerShell)
